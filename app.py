from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import uic
import sys
import datetime
from PyQt5.uic import loadUi
from cv2 import cv2
import mysql.connector
sys.path.append('../insightface/deploy')
sys.path.append('../insightface/src/common')
from mtcnn.mtcnn import MTCNN
from imutils import paths
import face_preprocess
import numpy as np
import argparse
import os
import faces_embedding
import train_softmax
import requests
import requests_cache
import hashlib 
#read path from file
f = open(".\\stringtoken.txt", "r")
line_1=f.readline()
line_2=f.readline()
len_line1=len(line_1)-1
len_line2=len(line_2)
f.close()
configue_string_url=line_1[20:len_line1]
string_token=line_2[13:len_line2]

configue_path_all_file=".\\"
path_image_data="..\\datasets\\train"
detector = MTCNN()
#main class
class __app__(QMainWindow):
    #khoi tao
    def __init__(self):
        super(__app__, self).__init__()
        global configue_path_all_file
        uic.loadUi(configue_path_all_file+'my_app.ui', self)
        self.type_frame=1
        self.hide_view()
        self.init_list()
        self.init_staff()
        #set style to UI
        self.center.setStyleSheet ( "background-image : url(./office.jpg);") 
        self.capture.setStyleSheet ("background: rgba(196,194,195,10);border: 8px solid rgb(255, 137, 139);background-image : url(./face.jpg);border-radius:15px;")
        self.b_add.setStyleSheet ("QPushButton::hover{background-color : lightgreen;}    QPushButton::pressed{background-color : red;}   QPushButton{border: 1px solid rgb(255, 137, 139);border-radius:15px;background: rgba(255, 255, 255, 150);}")
        self.b_train.setStyleSheet ("QPushButton::hover{background-color : lightgreen;}  QPushButton::pressed{background-color : red;}   QPushButton{border: 1px solid rgb(255, 137, 139);border-radius:15px;background: rgba(255, 255, 255, 150);}")
        self.b_search.setStyleSheet ("QPushButton::hover{background-color : lightgreen;} QPushButton::pressed{background-color : red;}   QPushButton{border: 1px solid rgb(255, 137, 139);border-radius:15px;background: rgba(255, 255, 255, 150);}")
        #processing Event
        self.l_api1.clicked.connect(self.search_api1)
        self.l_api2.clicked.connect(self.search_api2)
        self.l_api3.clicked.connect(self.search_api3)
        self.l_api4.clicked.connect(self.search_api4)
        self.l_api5.clicked.connect(self.search_api5)
        self.b_search.clicked.connect(self.call_api)
        self.b_add.clicked.connect(self.add)
        self.b_train.clicked.connect(self.train)
        self.l_edit_search.textEdited.connect(self.hide_view)
        self.l_edit_search.editingFinished.connect(self.call_api)
        self.rbtn1.toggled.connect(self.onRadioBtn1)
        self.rbtn2.toggled.connect(self.onRadioBtn2)
        self.show()
    #search từ CSDL
    def init_staff(self):
        self.account=""
        self.name=""
        self.mail=""
        self.user_id=""
    def init_list(self):
        self.list_id=[]
        self.list_name=[]
        self.list_user_id=[]
        self.list_mail=[]
        self.list_completer=[]
    #show dialog
    def showDialog(self,a,b):
        self.msgBox = QMessageBox()
        self.msgBox.setGeometry(600,300,300,100)
        self.msgBox.setIcon(QMessageBox.Information)
        self.msgBox.setText(a)
        self.msgBox.setWindowTitle(b)
        self.msgBox.setStandardButtons(QMessageBox.Ok)
        returnValue = self.msgBox.exec()
        if returnValue == QMessageBox.Ok:
            print('OK clicked')
    def load_label(self):
        types=self.type_frame
        if types==0:
            self.label.setText("Thông Tin Giảng Viên")
            self.label_2.setText("Mã Giảng Viên")
            self.label_3.setText("Tên Giảng Viên")
        else:
            self.label.setText("Thông Tin Học Viên")
            self.label_2.setText("Mã Học Viên")
            self.label_3.setText("Tên Học Viên")
    #function call api 
    def call_api(self):    
        self.l_api1.setText("")
        self.l_api2.setText("")
        self.l_api3.setText("")
        self.l_api4.setText("")
        self.l_api5.setText("")
        self.hide_view()
        self.account=self.l_edit_search.text()
        self.load_label()
        #infor api requests
        public_key=self.account + str(self.type_frame)+string_token
        print(public_key)
        token= hashlib.md5(public_key.encode('utf-8')).hexdigest()
        print(token)
        url = configue_string_url+"/api/students/search?q={0}&type={1}&token={2}".format(self.account,self.type_frame,token)
        try:
            response_dict = requests.get(url,verify=False).json()
            status_request=response_dict["status"]
            self.list_completer=[]
            self.init_list()
            if(status_request["code"]==200):
                data_user=response_dict["data"]
                try:
                    for i in data_user:
                        self.list_completer.append(str(i["name"]) +": "+str(i["account"]))
                        self.list_id.append(str(i["account"]))
                        self.list_name.append(str(i["name"]))
                        self.list_user_id.append(str(i["id"]))
                        self.list_mail.append(str(i['email']))
                except:
                    print("api tra ve null")
                leng_=len(self.list_completer)
                if leng_==0:
                    self.hide_view()               
                elif leng_==1:
                    self.control_complete1()
                elif leng_==2:
                    self.control_complete2()
                elif leng_==3:
                    self.control_complete3()
                elif leng_==4:
                    self.control_complete4()
                else:
                    self.control_complete5()
            else:
                self.init_staff()
        except:
            print("false api")
    #control completer
    def control_complete1(self):
        self.l_api1.show()
        self.l_api1.setText(self.list_completer[0])
    def control_complete2(self):
        self.control_complete1()
        self.l_api2.show()
        self.l_api2.setText(self.list_completer[1])
    def control_complete3(self):
        self.control_complete2()
        self.l_api3.show()
        self.l_api3.setText(self.list_completer[2])
    def control_complete4(self):
        self.control_complete3()
        self.l_api4.show()
        self.l_api4.setText(self.list_completer[3])
    def control_complete5(self):
        self.control_complete4()
        self.l_api5.show()
        self.l_api5.setText(self.list_completer[4])        
    #setText from api
    def api_setText(self):
        self.id_hocvien.setText(self.account)
        self.l_name.setText(self.name)
        self.l_mail.setText(self.mail)
        self.l_edit_search.setText(self.name)
        self.insert_staff()
        self.hide_view()
    #clear staff when search
    def clear_staff(self):
        self.id_hocvien.setText("")
        self.l_name.setText("")
        self.l_mail.setText("")
    #load data api
    def search_api(self,i):
        self.account=self.list_id[i]
        self.name=self.list_name[i]
        self.user_id=self.list_user_id[i]
        self.mail=self.list_mail[i]
        self.api_setText()
    def search_api1(self):
        self.search_api(0)
    def search_api2(self):
        self.search_api(1)
    def search_api3(self):
        self.search_api(2)
    def search_api4(self):
        self.search_api(3)
    def search_api5(self):
        self.search_api(4)
    #hide view completer
    def hide_view(self):
        self.l_api1.hide()
        self.l_api2.hide()
        self.l_api3.hide()
        self.l_api4.hide()
        self.l_api5.hide()         
    #insert staff to DB
    def insert_staff(self):
        try:
            mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                password="admin",
                database="diem_danh"
            )
            mycursor = mydb.cursor()
            sql_insert="insert into users(account,name,type,user_id) values(%(account)s,%(name)s,%(type)s,%(user_id)s);"
            data_insert = {
                    'account':self.account,
                    'name':self.name,
                    'type':self.type_frame,
                    'user_id':self.user_id,
                     }
            mycursor.execute(sql_insert,data_insert)                                    
            mydb.commit() 
            mydb.close()
        except:
            print("da them ")   
    #button check 
    def onRadioBtn1(self):
        rbtn=self.sender()
        if rbtn.isChecked():
            self.type_frame=0
            self.call_api()
            self.clear_staff()
    def onRadioBtn2(self):
        rbtn=self.sender()
        if rbtn.isChecked():
            self.type_frame=1
            self.call_api()
            self.clear_staff()
    #image collection
    def add(self):
        self.b_add.setEnabled(False)
        if self.account !="":
            self.b_add.setText(" ...")
            path_img=path_image_data+"\\"+self.account
            faces = 0
            frames = 0
            #  set number face 
            max_faces = 40       
            max_bbox = np.zeros(4)
            #read camera. if you want to read ip camera, change (0) in VideoCapture(0)
            cap = cv2.VideoCapture(0)
            if not cap.isOpened():
                print("Cannot open camera")
                exit()
            while faces < max_faces:
                # Capture frame-by-frame                                            
                ret, frame = cap.read()
                # if frame is read correctly ret is True
                if not ret:
                    print("Can't receive frame (stream end?). Exiting ...")
                    break           
                frames += 1
                bboxes = detector.detect_faces(frame)    
                # Our operations on the frame come here
                gray = cv2.cvtColor(frame, 1)

                if len(bboxes) != 0:
                # Get only the biggest face
                    max_area = 0
                    for bboxe in bboxes:
                        bbox = bboxe["box"]
                        bbox = np.array([bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3]])
                        keypoints = bboxe["keypoints"]
                        area = (bbox[2]-bbox[0])*(bbox[3]-bbox[1])
                        if area > max_area:
                            max_bbox = bbox
                            landmarks = keypoints
                            max_area = area
                    max_bbox = max_bbox[0:4]
                        # get face each of 3 frames
                    if frames%3 == 0:
                        # convert to face_preprocess.preprocess input
                        landmarks = np.array([landmarks["left_eye"][0], landmarks["right_eye"][0], landmarks["nose"][0], landmarks["mouth_left"][0], landmarks["mouth_right"][0],
                                            landmarks["left_eye"][1], landmarks["right_eye"][1], landmarks["nose"][1], landmarks["mouth_left"][1], landmarks["mouth_right"][1]])
                        landmarks = landmarks.reshape((2,5)).T
                        nimg = face_preprocess.preprocess(frame, max_bbox, landmarks, image_size='112,112')
                        if not(os.path.exists(path_img)):
                            os.makedirs(path_img)
                        cv2.imwrite(os.path.join(path_img, "{}.jpg".format(faces+1)), nimg)
                        cv2.rectangle(frame, (max_bbox[0], max_bbox[1]), (max_bbox[2], max_bbox[3]), (255, 0, 0), 2)
                        print("[INFO] {} faces detected".format(faces+1))
                        faces += 1
                        font = cv2.FONT_HERSHEY_SIMPLEX 
            
                        # org 
                        org = (300, 50)                         
                        # fontScale 
                        fontScale = 1                       
                        # Blue color in RGB
                        color = (255, 0, 0)                        
                        # Line thickness of 2 px 
                        thickness = 2
                        # Using cv2.putText() method 
                        frame = cv2.putText(frame, str(faces), org, font,  
                                        fontScale, color, thickness, cv2.LINE_AA)
                # Display the resulting frame
                gray=cv2.resize(frame,(500,500))
                self.displayImgage(gray,1)
                cv2.waitKey()
                
            # When everything done, release the capture
            cap.release()
            cv2.destroyAllWindows()
            self.b_add.setText("Thêm Ảnh")
            self.showDialog("Bạn đã lấy dữ liệu ảnh thành công", "Successful")
        else:
            self.showDialog("Bạn chưa nhập thông tin"," ")
        self.b_add.setEnabled(True)

    #train models
    def Training_model(self):
        faces_embedding.compress_data()
        train_softmax.train_s() 
        self.b_train.setEnabled(True)
        self.b_train.setText("Huấn luyện mô hình")
        print("train success")
        self.notify.setText("HUẤN LUYỆN ĐÃ HOÀN THÀNH")
    #button train
    def train(self): 
        self.b_train.setText("...")
        self.notify.setText("ĐANG HUẤN LUYỆN MÔ HÌNH, VUI LÒNG ĐỢI !!!")
        self.b_train.setEnabled(False)
        num_file=os.listdir(path_image_data)
        print(len(num_file))
        a=len(num_file)
        if a>=2:
            # self.thread1=QThreadPool()
            self.thread2=QThreadPool()
            # self.thread1.start(self.setTextTrain)
            self.thread2.start(self.Training_model) 
        else:
            self.showDialog("Bạn cần ít nhất 2 người ","Successful")
            self.b_train.setEnabled(True)
            self.b_train.setText("Huấn luyện mô hình")
            self.notify.setText("")

    #display camera 
    def displayImgage(self,img,window=1):
        qformat=QImage.Format_Indexed8
        if len(img.shape)==3:
            if(img.shape[2])==4:
                qformat=QImage.Format_RGB888
            else:
                qformat=QImage.Format_RGB888
        img=QImage(img,img.shape[1],img.shape[0],qformat)
        img=img.rgbSwapped()
        self.capture.setPixmap(QPixmap.fromImage(img))
app = QApplication(sys.argv)
window = __app__()
sys.exit(app.exec_())
