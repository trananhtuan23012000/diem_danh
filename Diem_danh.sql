CREATE DATABASE  IF NOT EXISTS `diem_danh` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `diem_danh`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: diem_danh
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `checkin`
--

DROP TABLE IF EXISTS `checkin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `checkin` (
  `id` varchar(45) NOT NULL,
  `date_checkin` date NOT NULL,
  `time_checkin` time NOT NULL,
  `status` int DEFAULT NULL,
  `account` varchar(45) NOT NULL,
  PRIMARY KEY (`date_checkin`,`time_checkin`,`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkin`
--

LOCK TABLES `checkin` WRITE;
/*!40000 ALTER TABLE `checkin` DISABLE KEYS */;
INSERT INTO `checkin` VALUES ('18','2020-10-15','16:58:49',NULL,'ZAD0000018'),('90','2020-10-15','17:44:07',NULL,'Z000000090'),('26','2020-10-15','18:52:08',NULL,'ZAD0000026'),('545','2020-10-15','18:53:56',NULL,'Z000000545'),('552','2020-10-15','18:54:25',NULL,'Z000000552'),('546','2020-10-15','18:54:48',NULL,'Z000000546'),('570','2020-10-15','18:55:24',NULL,'Z000000570'),('534','2020-10-15','18:59:31',NULL,'Z000000534'),('52','2020-10-15','19:09:38',NULL,'Z000000052'),('96','2020-10-15','19:10:04',NULL,'Z000000096'),('527','2020-10-15','19:10:51',NULL,'Z000000527'),('521','2020-10-15','19:11:07',NULL,'Z000000521'),('528','2020-10-15','19:11:33',NULL,'Z000000528'),('19','2020-10-15','19:21:32',NULL,'ZAD0000019'),('22','2020-10-15','19:22:06',NULL,'ZAD0000022');
/*!40000 ALTER TABLE `checkin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `account` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `type` int NOT NULL,
  `user_id` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`,`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('Z000000104','Phạm Thị Thúy',NULL,1,'104'),('ZAD0000018','Vũ Hoài Nam',NULL,0,'18'),('ZAD0000019','Vũ Thị Uyên',NULL,0,'19'),('ZAD0000022','Mẫn Văn Hậu',NULL,0,'22'),('ZAD0000026','Vũ Nam Hưng',NULL,0,'26'),('Z000000052',' Lò Tuấn Nam',NULL,1,'52'),('Z000000521','Le Quang Hung',NULL,1,'521'),('Z000000527','Nguyễn Ngọc Phong',NULL,1,'527'),('Z000000528','Nguyễn Tất Thành',NULL,1,'528'),('Z000000534','Nguyễn Thị Sinh',NULL,1,'534'),('Z000000539','Dương Trọng Đức',NULL,1,'539'),('Z000000545','Đặng Quốc Tiến',NULL,1,'545'),('Z000000546','Dương Đức Huỳnh',NULL,1,'546'),('Z000000552','Nguyễn Đức Hiếu',NULL,1,'552'),('Z000000570','Phạm Văn Duy',NULL,1,'570'),('ZAD0000007','Trần Minh Đức',NULL,0,'7'),('ZAD0000009','Nguyễn Công Hoan',NULL,0,'9'),('Z000000090','Nguyễn Quang Thành',NULL,1,'90'),('Z000000096','Lê Ngọc Luân',NULL,1,'96');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'diem_danh'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-16  8:56:53
