from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import uic
import sys
import hashlib
import datetime
from datetime import datetime
import keras
from tensorflow.keras.models import load_model
from PyQt5.uic import loadUi
from cv2 import cv2
import mysql.connector
sys.path.append('../insightface/deploy')
sys.path.append('../insightface/src/common')
from mtcnn.mtcnn import MTCNN
from imutils import paths
import face_preprocess
import numpy as np
import argparse
import face_model
import os
import pickle
import time
import dlib
import faces_embedding
import train_softmax
from gtts import gTTS 
import playsound
from pathlib import Path
import requests
#read path from file
f = open(".\\stringtoken.txt", "r")
line_1=f.readline()
line_2=f.readline()
len_line1=len(line_1)-1
len_line2=len(line_2)
f.close()
configue_string_url=line_1[20:len_line1]
string_token=line_2[13:len_line2]
print(configue_string_url)
url_post = configue_string_url+"/api/attendance/checkin"
print(url_post)
#time to recheckin
time_study=3600
number_staff=25
configue_path_all_file=".\\"
# Define distance function
detector = MTCNN()
#read camera
cap=cv2.VideoCapture(0)
#load scree res
screen_res = QApplication(sys.argv)
rect = screen_res.primaryScreen().availableGeometry()
width=rect.width()
height=rect.height()
#Calculate cosine distance between two vector
def findCosineDistance(vector1, vector2):
    vec1 = vector1.flatten()
    vec2 = vector2.flatten()
    a = np.dot(vec1.T, vec2)
    b = np.dot(vec1.T, vec1)
    c = np.dot(vec2.T, vec2)
    return 1 - (a/(np.sqrt(b)*np.sqrt(c)))
#Verify the similarity of one vector to group vectors of one class
def CosineSimilarity(test_vec, source_vecs):
    cos_dist = 0
    for source_vec in source_vecs:
        cos_dist += findCosineDistance(test_vec, source_vec)
    return cos_dist/len(source_vecs)
#show dialog
def showDialog(a,b):
    msgBox = QMessageBox()
    msgBox.setGeometry(570,320,300,100)
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setText(a)
    msgBox.setWindowTitle(b)
    msgBox.setStandardButtons(QMessageBox.Ok)
    returnValue = msgBox.exec()
    if returnValue == QMessageBox.Ok:
        print('OK clicked')
#main Class
class __diem_danh__(QMainWindow):
    def __init__(self):
        super(__diem_danh__, self).__init__()
        self.id_student=""
        uic.loadUi(configue_path_all_file+'diem_danh.ui', self)
        self.list_staff.setGeometry(0,0,0.25*width,height)
        self.cam.setGeometry(0.25*width,0,0.75*width,height)
        self.list_staff_show=""
        self.showMaximized()
        self.thread1=QThreadPool()
        self.thread2=QThreadPool()
        self.thread1.start(self.load_camera)
        self.thread2.start(self.proccessing)

    #load camera to app - thread 1
    def load_camera(self):
        if not cap.isOpened():
            print("Cannot open camera")
            exit()
        detector1=MTCNN()
        while True:
             #Capture frame-by-frame
            __, frame = cap.read()
            
            #Use MTCNN to detect faces
            result = detector1.detect_faces(frame)
            if result != []:
                for person in result:
                    bounding_box = person['box']
                    keypoints = person['keypoints']
            
                    cv2.rectangle(frame,
                                (bounding_box[0], bounding_box[1]),
                                (bounding_box[0]+bounding_box[2], bounding_box[1] + bounding_box[3]),
                                (124,252,0),
                                2 )            
            gray = cv2.cvtColor(frame, 1)
            gray=cv2.resize(frame,(int(0.75*width),height),interpolation= cv2.INTER_AREA)
            self.displayImgage(gray,1)
            cv2.waitKey(0)
            if cv2.waitKey(1) == ord('a'):
                break       

   #processing camera -thread2
    def proccessing(self):
        os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"]="0,3"
        ap = argparse.ArgumentParser()
        ap.add_argument("--mymodel", default="outputs/my_model.h5",
            help="Path to recognizer model")
        ap.add_argument("--le", default="outputs/le.pickle",
            help="Path to label encoder")
        ap.add_argument("--embeddings", default="outputs/embeddings.pickle",
            help='Path to embeddings')
        ap.add_argument("--video-out", default="../datasets/videos_output/stream_test.mp4",
            help='Path to output video')

        ap.add_argument('--image-size', default='112,112', help='')
        ap.add_argument('--model', default='../insightface/models/model-y1-test2/model,0', help='path to load model.')
        ap.add_argument('--ga-model', default='', help='path to load model.')
        ap.add_argument('--gpu', default=0, type=int, help='gpu id')
        ap.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
        ap.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
        ap.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')

        args = ap.parse_args()
        # Load embeddings and labels
        data = pickle.loads(open(args.embeddings, "rb").read())
        le = pickle.loads(open(args.le, "rb").read())
        embeddings = np.array(data['embeddings'])
        labels = le.fit_transform(data['names'])
        # Initialize detector
        detector = MTCNN()
        # Initialize faces embedding model
        embedding_model =face_model.FaceModel(args)
        # Load the classifier model
        model = load_model('outputs/my_model.h5')
        # Initialize some useful arguments
        cosine_threshold = 0.85
        proba_threshold = 0.93
        comparing_num = 5
        trackers = []
        texts = []
        frames = 0
        kq=np.zeros(len(embeddings), dtype=int)

        frame_width = int(cap.get(3))
        frame_height = int(cap.get(4))
        save_width = 1000
        save_height = 720
        text_recent= "Unknown"
        dem=0
        while True:
            ret, frame = cap.read()
            frames += 1
            rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = cv2.resize(frame, (save_width, save_height))
            gray = cv2.cvtColor(frame, 1)
            gray=cv2.resize(frame,(int(0.75*width),height),interpolation= cv2.INTER_AREA)
            if frames%3 == 0 :
                trackers = []
                texts = []
                #detect_tick = time.time()
                bboxes = detector.detect_faces(frame)
                #detect_tock = time.time()
                if len(bboxes) != 0:
                    #reco_tick = time.time()
                    for bboxe in bboxes:
                        bbox = bboxe['box']
                        bbox = np.array([bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3]])
                        landmarks = bboxe['keypoints']
                        landmarks = np.array([landmarks["left_eye"][0], landmarks["right_eye"][0], landmarks["nose"][0], landmarks["mouth_left"][0], landmarks["mouth_right"][0],
                                            landmarks["left_eye"][1], landmarks["right_eye"][1], landmarks["nose"][1], landmarks["mouth_left"][1], landmarks["mouth_right"][1]])
                        landmarks = landmarks.reshape((2,5)).T
                        nimg = face_preprocess.preprocess(frame, bbox, landmarks, image_size='112,112')
                        nimg = cv2.cvtColor(nimg, cv2.COLOR_BGR2RGB)
                        nimg = np.transpose(nimg, (2,0,1))
                        embedding = embedding_model.get_feature(nimg).reshape(1,-1)
                        text_recent="Unknown"
                        # Predict class
                        preds = model.predict(embedding)
                        preds = preds.flatten()
                        # Get the highest accuracy embedded vector
                        j = np.argmax(preds)
                        proba = preds[j]
                        # Compare this vector to source class vectors to verify it is actual belong to this class
                        match_class_idx = (labels == j)
                        match_class_idx = np.where(match_class_idx)[0]
                        selected_idx = np.random.choice(match_class_idx, comparing_num)
                        compare_embeddings = embeddings[selected_idx]
                        # Calculate cosine similarity
                        cos_similarity = CosineSimilarity(embedding, compare_embeddings)
                        if cos_similarity < cosine_threshold and proba >= proba_threshold:
                            name = le.classes_[j]
                            text_recent = "{}".format(name)
                            print("Recognized: {} <{:.2f}>".format(name, proba*100))
                            kq[j]+=1

                        # Start tracking
                        tracker = dlib.correlation_tracker()
                        rect = dlib.rectangle(bbox[0], bbox[1], bbox[2], bbox[3])
                        tracker.start_track(rgb, rect)
                        trackers.append(tracker)
                        texts.append(text_recent)
            else:
                for tracker, text in zip(trackers,texts):
                    pos = tracker.get_position()
                    # unpack the position object
                    startX = int(pos.left())
                    startY = int(pos.top())
                    endX = int(pos.right())
                    endY = int(pos.bottom())

            #confirm student or teacher      
            for i in range(len(embeddings)):
                if kq[i]>=5:
                    kq[i]=0
                    #connect mysql db
                    mydb = mysql.connector.connect(
                    host="localhost",
                    user="root",
                    password="admin",
                    database="diem_danh"
                        )
                    mycursor = mydb.cursor()
                    a=0
                    #querry infor student
                    try:
                        sql_check="SELECT * FROM users where account=%(account)s ;"
                        data_check = {
                            'account':text_recent,
                        }
                        mycursor.execute(sql_check, data_check)
                        myresult=mycursor.fetchall()
                        print(myresult)
                        #save infor checkin
                        user_type=myresult[0][3]
                        user_checkin=myresult[0][1]
                        account=myresult[0][0]
                        user_id=myresult[0][4]
   
                        named_tuple = time.localtime() # lấy struct_time
                        time_now=time.strftime("%Y-%m-%d %H:%M:%S")
                        date = time.strftime("%Y-%m-%d", named_tuple)
                        timec=time.strftime("%H:%M:%S",named_tuple)

                        #querry last time checkin
                        sql_last_check="SELECT MAX(time_checkin) FROM checkin WHERE account=%(account)s and date_checkin=%(date_checkin)s ; "
                        data_last_check={
                            'account':text_recent,
                            'date_checkin':date,
                        }
                        mycursor.execute(sql_last_check,data_last_check)
                        myresult=mycursor.fetchall()
                        # print(myresult)
                        delta_time=0
                        if myresult[0][0] is None:
                            delta_time=0
                        else:                   
                            #convert time from mysql to time python
                            last_time_checkin = datetime.strptime(str(myresult[0][0]), '%H:%M:%S')
                            format1 = '%H:%M:%S'
                            delta_time=datetime.strptime(timec, format1) - datetime.strptime(str(myresult[0][0]), '%H:%M:%S')
                            #convert time to timestamp
                            delta_time=delta_time.total_seconds()
                        
                        #distance checkin is 1 hour
                        if myresult[0][0] is None or delta_time>= time_study:
                            # insert to DB
                            sql_checkin="insert into checkin(id ,date_checkin,time_checkin,account) values(%(id)s,%(date_checkin)s,%(time_checkin)s,%(account)s);"
                            data_checkin = {
                            'id':user_id,
                            'date_checkin':date,
                            'time_checkin':timec,
                            'account': account,
                            }
                            mycursor.execute(sql_checkin,data_checkin)
                            mydb.commit() 
                            mydb.close()
    
                            #send request to API
                            public_key=account + user_id + time_now + str(user_type)+ string_token
                            # print(public_key)
                            token= hashlib.md5(public_key.encode('utf-8')).hexdigest()
                            # print(token)
                            global url_post
                            datas = {
                                    'account':account,
                                    'user_id':user_id,
                                    'time':time_now, 
                                    'type':user_type,
                                    'token':token
                                    } 
                            #send request
                            x = requests.post(url_post, data = datas,verify=False)
                            # print(x.text)
                            dem=dem+1
                            staff=""
                            b=0
                            #speak hello student
                            try:
                                staff=str(time_now) + " "+ str(user_checkin)
                                app_speak="Xin Chào" + str(user_checkin)
                                tts=gTTS(text=app_speak,lang='vi')
                                path_voice=configue_path_all_file+"outputs\\audio\\"+str(text_recent)+".mp3"
                                my_file = Path(path_voice)
                                
                                if my_file.is_file():
                                    playsound.playsound(path_voice)
                                    self.list_staff_show=self.list_staff_show +"\n"+staff 
                                else:
                                    tts.save(path_voice)
                                    playsound.playsound(path_voice)
                                    self.list_staff_show=self.list_staff_show+"\n" +staff
                                # print(self.list_staff_show)
                                self.list_staff.setPlaceholderText(self.list_staff_show)
                            except:
                                print("fail hello")
                            staff=""
                    except:
                        print("fail checkin")
                    if dem % number_staff == 0:
                        self.list_staff_show=""
                    # print(myresult)
                    text_recent="Unknown"
                    break        
        cap.release()
        cv2.destroyAllWindows()
        showDialog("Bạn đã lấy dữ liệu ảnh thành công", "Successful")
    #load camera to frame
    def displayImgage(self,img,window=1):
        qformat=QImage.Format_Indexed8
        if len(img.shape)==3:
            if(img.shape[2])==4:
                qformat=QImage.Format_RGB888
            else:
                qformat=QImage.Format_RGB888
        img=QImage(img,img.shape[1],img.shape[0],qformat)
        img=img.rgbSwapped()
        self.cam.setPixmap(QPixmap.fromImage(img))
app = QApplication(sys.argv)
window = __diem_danh__()
sys.exit(app.exec_())
